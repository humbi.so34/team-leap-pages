function updateContent(index) {
  var descriptions = [
    "Der Raspberry Pi der 3. Generation ist das Herzstück unseres Rocket Deployer, über ihn laufen alle RFID Reader und Inputs.",
    "Die soliden MFRC522 Reader lesen zuverlässig die RFID Karten.",
    "Die RFID Karten repräsentieren die verschiedenen Module der Applikation.",
    "Description for Apple Watch"
  ];

  var descriptionElement = document.getElementById('description');
  var dots = document.querySelectorAll('.controller.dots li');

  // Update the description
  descriptionElement.textContent = descriptions[index];

  // Update the selected dot
  dots.forEach(function(dot, i) {
    if (i === index) {
      dot.classList.add('selected');
    } else {
      dot.classList.remove('selected');
    }
  });
}